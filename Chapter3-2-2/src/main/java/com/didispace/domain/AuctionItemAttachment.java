package com.didispace.domain;

import lombok.Getter;
import lombok.Setter;
import oracle.sql.BLOB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/25 13:27
 * @Description:
 */
@Entity
@Table(name = "CND_AUCTION_ITEM_ATTACHMENT")
public class AuctionItemAttachment {
    @Id @Setter @Getter
    private String ID;
    @Column @Setter @Getter
    private String NAME;
    @Column @Setter @Getter
    private String CONTENTTYPE;
    @Column @Setter @Getter
    private BLOB ATTACHMENT;
    @Column @Setter @Getter
    private String  ITEMID;

}
