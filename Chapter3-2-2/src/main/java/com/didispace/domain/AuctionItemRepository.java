package com.didispace.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:00
 * @Description:
 */
public interface AuctionItemRepository extends JpaRepository<Auctionitem, String> {

}
