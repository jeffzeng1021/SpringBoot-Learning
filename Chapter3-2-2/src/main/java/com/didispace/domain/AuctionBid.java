package com.didispace.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/25 12:43
 * @Description:
 */
@Entity
@Table(name = "CND_AUCTIONBIDS")

public class AuctionBid {

    @Id
    @Getter
    @Setter
    private String BID;
    @Column
    @Getter
    @Setter
    private String ITEMID;
    @Column
    @Getter
    @Setter
    private BigDecimal BIDAMOUNT;
    @Column
    @Getter
    @Setter
    private Date BIDDATE;
    @Column
    @Getter
    @Setter
    private String USERID;
    @Column
    @Getter
    @Setter
    private String USERNAME;
    @Column
    @Getter
    @Setter
    private String IP;
    @Column
    @Getter
    @Setter
    private BigDecimal COMPANYID;
    @Column
    @Getter
    @Setter
    private BigDecimal GROUPID;

}
