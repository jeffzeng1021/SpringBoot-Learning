package com.didispace.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="CND_AUCTIONITEMS")
public class Auctionitem {
    @Id
    private String ITEMID;
    @Column
    private BigDecimal GROUPID;
    @Column
    private BigDecimal USERID;
    @Column
    private String ITEMTITLE;
    @Column
    private String STATE;
    @Column
    private String CATEGORYID;
    @Column
    private Date AVAILDATE;
    @Column
    private Date CLOSEDATE;
    @Column
    private BigDecimal STARTINGBID;
    @Column
    private BigDecimal MINIMUMINCREMENT;
    @Column
    private String DESCRIPTIONS;
    @Column
    private BigDecimal ISSHOW;
    @Column
    private String USERNAME;
    @Column
    private String CATEGORYNAME;
    @Column
    private BigDecimal COMPANYID;
    @Column
    private BigDecimal RECORDVERSION;
    @Column
    private BigDecimal BIDCOUNT;
    @Column
    private BigDecimal LASTBID;
    @Column
    private Date INITCLOSEDATE;
    @Column
    private BigDecimal VERSION;

    @Override
    public String toString() {
        return "Auctionitem [ITEMID=" + ITEMID + ", GROUPID=" + GROUPID + ", USERID=" + USERID + ", ITEMTITLE=" + ITEMTITLE + ", STATE=" + STATE + ", CATEGORYID=" + CATEGORYID
                + ", AVAILDATE=" + AVAILDATE + ", CLOSEDATE=" + CLOSEDATE + ", STARTINGBID=" + STARTINGBID + ", MINIMUMINCREMENT=" + MINIMUMINCREMENT + ", DESCRIPTIONS="
                + DESCRIPTIONS + ", ISSHOW=" + ISSHOW + ", USERNAME=" + USERNAME + ", CATEGORYNAME=" + CATEGORYNAME + ", COMPANYID=" + COMPANYID + ", RECORDVERSION="
                + RECORDVERSION + ", BIDCOUNT=" + BIDCOUNT + ", LASTBID=" + LASTBID + ", INITCLOSEDATE=" + INITCLOSEDATE + ", VERSION=" + VERSION + "]";
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String iTEMID) {
        ITEMID = iTEMID;
    }

    public BigDecimal getGROUPID() {
        return GROUPID;
    }

    public void setGROUPID(BigDecimal gROUPID) {
        GROUPID = gROUPID;
    }

    public BigDecimal getUSERID() {
        return USERID;
    }

    public void setUSERID(BigDecimal uSERID) {
        USERID = uSERID;
    }

    public String getITEMTITLE() {
        return ITEMTITLE;
    }

    public void setITEMTITLE(String iTEMTITLE) {
        ITEMTITLE = iTEMTITLE;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String sTATE) {
        STATE = sTATE;
    }

    public String getCATEGORYID() {
        return CATEGORYID;
    }

    public void setCATEGORYID(String cATEGORYID) {
        CATEGORYID = cATEGORYID;
    }

    public Date getAVAILDATE() {
        return AVAILDATE;
    }

    public void setAVAILDATE(Date aVAILDATE) {
        AVAILDATE = aVAILDATE;
    }

    public Date getCLOSEDATE() {
        return CLOSEDATE;
    }

    public void setCLOSEDATE(Date cLOSEDATE) {
        CLOSEDATE = cLOSEDATE;
    }

    public BigDecimal getSTARTINGBID() {
        return STARTINGBID;
    }

    public void setSTARTINGBID(BigDecimal sTARTINGBID) {
        STARTINGBID = sTARTINGBID;
    }

    public BigDecimal getMINIMUMINCREMENT() {
        return MINIMUMINCREMENT;
    }

    public void setMINIMUMINCREMENT(BigDecimal mINIMUMINCREMENT) {
        MINIMUMINCREMENT = mINIMUMINCREMENT;
    }

    public String getDESCRIPTIONS() {
        return DESCRIPTIONS;
    }

    public void setDESCRIPTIONS(String dESCRIPTIONS) {
        DESCRIPTIONS = dESCRIPTIONS;
    }

    public BigDecimal getISSHOW() {
        return ISSHOW;
    }

    public void setISSHOW(BigDecimal iSSHOW) {
        ISSHOW = iSSHOW;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        USERNAME = uSERNAME;
    }

    public String getCATEGORYNAME() {
        return CATEGORYNAME;
    }

    public void setCATEGORYNAME(String cATEGORYNAME) {
        CATEGORYNAME = cATEGORYNAME;
    }

    public BigDecimal getCOMPANYID() {
        return COMPANYID;
    }

    public void setCOMPANYID(BigDecimal cOMPANYID) {
        COMPANYID = cOMPANYID;
    }

    public BigDecimal getRECORDVERSION() {
        return RECORDVERSION;
    }

    public void setRECORDVERSION(BigDecimal rECORDVERSION) {
        RECORDVERSION = rECORDVERSION;
    }

    public BigDecimal getBIDCOUNT() {
        return BIDCOUNT;
    }

    public void setBIDCOUNT(BigDecimal bIDCOUNT) {
        BIDCOUNT = bIDCOUNT;
    }

    public BigDecimal getLASTBID() {
        return LASTBID;
    }

    public void setLASTBID(BigDecimal lASTBID) {
        LASTBID = lASTBID;
    }

    public Date getINITCLOSEDATE() {
        return INITCLOSEDATE;
    }

    public void setINITCLOSEDATE(Date iNITCLOSEDATE) {
        INITCLOSEDATE = iNITCLOSEDATE;
    }

    public BigDecimal getVERSION() {
        return VERSION;
    }

    public void setVERSION(BigDecimal vERSION) {
        VERSION = vERSION;
    }

}
