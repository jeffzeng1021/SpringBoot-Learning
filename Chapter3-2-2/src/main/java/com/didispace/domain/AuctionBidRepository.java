package com.didispace.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:00
 * @Description:
 */
public interface AuctionBidRepository extends JpaRepository<AuctionBid, String> {

    @Query("from AuctionBid u where u.BIDAMOUNT = (select max(t.BIDAMOUNT) from AuctionBid t where t.ITEMID=:ITEMID) and u.ITEMID=:ITEMID ")
    AuctionBid findMaxBIDAMOUNT(@Param("ITEMID") String ITEMID);

}
