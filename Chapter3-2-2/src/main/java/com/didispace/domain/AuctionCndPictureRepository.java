package com.didispace.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:00
 * @Description:
 */
public interface AuctionCndPictureRepository extends JpaRepository<AuctionCndPictrure, String> {


}
