package com.didispace.domain;

import lombok.Getter;
import lombok.Setter;
import oracle.sql.BLOB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/25 13:27
 * @Description:
 */
@Entity
@Table(name = "CND_PICTURE")
public class AuctionCndPictrure {
    @Id @Setter @Getter
    private String PICTUREID;
    @Column @Setter @Getter
    private String PICTURENAME;
    @Column @Setter @Getter
    private BLOB PICTURE;
    @Column @Setter @Getter
    private BigDecimal COMPANYID;
    @Column @Setter @Getter
    private String  ITEMID;

}
