package com.didispace.web;

import com.alibaba.fastjson.JSONObject;
import com.didispace.domain.*;
import com.didispace.dto.ErrorInfo;
import com.didispace.web.model.*;
import com.didispace.exception.MyException;
import com.didispace.service.FileService;
import com.didispace.util.DAO;
import com.didispace.util.Tools;
import com.google.common.io.BaseEncoding;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 19:08
 * @Description:
 */
@Slf4j
@RestController
public class AuctionController {




    @Autowired
    private FileService fileService;

    @Autowired
    private AuctionItemRepository auctionItemRepository;

    @Autowired
    private AuctionBidRepository auctionBidRepository;



    @RequestMapping(value = "/putAuctions", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ErrorInfo putAuctions(@RequestBody List<AuctionItemModel> auctionItemModels) throws MyException {
        ErrorInfo<List> r = new ErrorInfo<List>();
        r.setUrl("/putAuctions");
        List<Map> relationList = new ArrayList();
        Map relationMap;
        Auctionitem auctionitem;
        final DAO dao = new DAO();
        try {
            for (int i = 0; i < auctionItemModels.size(); i++) {
                AuctionItemModel auctionItemModel = auctionItemModels.get(i);
                auctionitem = new Auctionitem();
                auctionitem.setAVAILDATE(new Date(auctionItemModel.getStartTime()));
                auctionitem.setGROUPID(new BigDecimal(58341));
                auctionitem.setUSERID(new BigDecimal(22984886));
                auctionitem.setCATEGORYID("2c9080d11dfcdf6c011e04d4c5960e65");
                auctionitem.setISSHOW(new BigDecimal(1));
                auctionitem.setCATEGORYNAME("受托拍卖");
                auctionitem.setSTATE("0");
                auctionitem.setRECORDVERSION(new BigDecimal(0));
                auctionitem.setBIDCOUNT(new BigDecimal(0));
                auctionitem.setVERSION(new BigDecimal(0));
                auctionitem.setCOMPANYID(new BigDecimal(58201));
                auctionitem.setITEMID(Tools.getUUID());
                auctionitem.setITEMTITLE(auctionItemModel.getGoodName());
                auctionitem.setCLOSEDATE(new Date(auctionItemModel.getEndTime()));
                auctionitem.setSTARTINGBID(new BigDecimal(auctionItemModel.getStartPrice()));
                auctionitem.setMINIMUMINCREMENT(new BigDecimal(auctionItemModel.getMinIncrease()));
                auctionitem.setDESCRIPTIONS(auctionItemModel.getDescription());
                auctionitem = auctionItemRepository.save(auctionitem);
                //保存拍品单位对应
                if(!auctionItemModel.getAuctionUnit().isEmpty()){
                    dao.update("insert into CND_AUCTION_ITEM_UNIT values(?,?)",auctionitem.getITEMID(),auctionItemModel.getAuctionUnit());
                }
                String fileName;
                String imageData;
                byte[] imageByte;
                //保存图片数据
                List<ImageModel> list = auctionItemModel.getList();

                for (int j = 0; j < list.size(); j++) {
                    fileName =  list.get(j).getFileName();
                    imageData = list.get(j).getImageData();
//                    imageByte = Tools.HexStringToBinary(imageData);
                    //解码

                    byte[] decodeBuffer = Tools.StringDecode(imageData);
//                    Tools.writeFileUseByte(decodeBuffer);
//                    fileService.executeUpload2OrclUseByte(auctionitem.getITEMID(),fileName,decodeBuffer);
//                    fileService.uploadFile2OrclUseDir(auctionitem.getITEMID(),fileName,decodeBuffer);
                    dao.inertBlob2Orcl(auctionitem.getITEMID(),fileName,decodeBuffer);
                }


                //保存成功，加入到relationMap中
                relationMap = new HashMap();
                relationMap.put("auctionGoodId", auctionitem.getITEMID());
                relationMap.put("goodId", auctionItemModel.getGoodId());
                relationList.add(relationMap);
            }
            log.info(JSONObject.toJSONString(relationList));
            r.setData(relationList);
            r.setCode(ErrorInfo.OK);
            r.setMessage("拍品上传成功!");
//            return JSONObject.toJSONString(r);
            return r;
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("拍品上传失败!");
        }
    }

//    @RequestMapping(value = "/putAuctionsNew", method = RequestMethod.POST)
//    @ResponseBody
//    @Transactional
//    public ErrorInfo putAuctionsNew(@RequestBody List<AuctionItemModelNew> auctionItemModels) throws MyException {
//        ErrorInfo<List> r = new ErrorInfo<List>();
//        BaseEncoding.base64().encode();
//        System.out.println(auctionItemModels.get(0).getList().toString().getBytes());
//        return r;
//    }

    /**
     *  文件上传
     * @return
     */
    @RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ErrorInfo uploadFiles(@RequestParam("auctionGoodId") String auctionGoodId,
                                 @RequestParam("file")MultipartFile file) throws MyException {
////    public ErrorInfo uploadFiles(@RequestBody UploadFileModel uploadFileModel) throws MyException {
        ErrorInfo<List> r = new ErrorInfo<List>();
        r.setUrl("/uploadFiles");
//        MultipartFile[] files = uploadFileModel.getFiles();
        UploadFileModel uploadFileModel = new UploadFileModel();
        uploadFileModel.setAuctionGoodId(auctionGoodId);
        uploadFileModel.setFiles(file);
        try {
            fileService.executeUpload2Orcl(uploadFileModel.getAuctionGoodId(),uploadFileModel.getFiles());
//            for (int i = 0; i < files.length; i++) {
//                if (files[i] != null) {
//                    //调用上传方法
//                    fileService.executeUpload2Orcl(uploadFileModel.getAuctionGoodId(),files[i]);
//                }
//            }
            r.setCode(ErrorInfo.OK);
            r.setMessage("图片上传成功!");
            return r;

        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException("图片上传失败!");
        }
    }

    @RequestMapping(value = "/getAuctionState/{auctionGoodId}", method = RequestMethod.GET)
    @ResponseBody
    public ErrorInfo getAuctionState(@PathVariable String auctionGoodId) throws MyException {
        ErrorInfo<AuctionItemStateModel> r = new ErrorInfo<AuctionItemStateModel>();
        r.setUrl("/getAuctionState");
        AuctionItemStateModel auctionItemStateModel = new AuctionItemStateModel();
       try {
           Auctionitem auctionitem = auctionItemRepository.getOne(auctionGoodId);
           if(auctionitem.getITEMID()==null){
               r.setCode(ErrorInfo.ERROR);
               r.setMessage("请传入正确的拍品ID!");
               return  r;
           }
           auctionItemStateModel.setAuctionGoodId(auctionitem.getITEMID());
           auctionItemStateModel.setPriceTimes(auctionitem.getBIDCOUNT());

           //当状态等于0时，判断当前时间是否小于于拍卖开始时间，是的话保持0，否的话保持改为2
           if(auctionitem.getSTATE().equals("0")){
               Date d = new Date();
               if(d.before(auctionitem.getAVAILDATE())){
                   auctionItemStateModel.setState(auctionitem.getSTATE());
               }
               else{
                   auctionItemStateModel.setState("2");
               }
           }
           else if(auctionitem.getSTATE().equals("1")){
               Date d = new Date();
               if(d.after(auctionitem.getAVAILDATE())&&d.before(auctionitem.getCLOSEDATE())){
                   auctionItemStateModel.setState(auctionitem.getSTATE());
               }
               else {
                   log.info("====="+auctionitem.getITEMID()+"被手动重置为2====");
                   auctionItemStateModel.setState("2");
               }
           }else{
               auctionItemStateModel.setState(auctionitem.getSTATE());
           }
           if(auctionitem.getBIDCOUNT()!=null&&auctionitem.getBIDCOUNT().doubleValue()>0){
                //查询拍卖情况表
               AuctionBid auctionBid = auctionBidRepository.findMaxBIDAMOUNT(auctionGoodId);
               auctionItemStateModel.setDealTime(auctionBid.getBIDDATE().getTime());
               auctionItemStateModel.setDealPrice(auctionBid.getBIDAMOUNT());
               auctionItemStateModel.setDealUserName(auctionBid.getUSERNAME());
           }
           log.info(JSONObject.toJSONString(auctionItemStateModel));
            r.setData(auctionItemStateModel);
            r.setCode(ErrorInfo.OK);
            r.setMessage("获取拍卖结果状态成功!");
            return  r;
        } catch (Exception e) {
            throw new MyException("查询出现异常!");
        }
    }
}
