package com.didispace.web.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:30
 * @Description:
 */
public class AuctionItemModel {

    private String goodId;
    private String goodName;
    private long startTime;
    private long endTime;
    private double startPrice;
    private double minIncrease;
    private String auctionUnit;
    private String description;
    private String photos;

    @Setter
    @Getter
    private List<ImageModel> list;

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setMinIncrease(double minIncrease) {
        this.minIncrease = minIncrease;
    }

    public double getMinIncrease() {
        return minIncrease;
    }

    public void setAuctionUnit(String auctionUnit) {
        this.auctionUnit = auctionUnit;
    }

    public String getAuctionUnit() {
        return auctionUnit;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getPhotos() {
        return photos;
    }
}
