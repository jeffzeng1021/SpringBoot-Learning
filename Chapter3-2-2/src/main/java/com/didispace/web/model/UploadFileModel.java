package com.didispace.web.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/25 13:15
 * @Description:
 */
public class UploadFileModel {
    @Setter @Getter
    private String auctionGoodId;
    @Setter @Getter
    private MultipartFile files;
}