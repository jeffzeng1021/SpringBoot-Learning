package com.didispace.web.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:30
 * @Description:
 */
public class AuctionItemModelNew {

        private String goodId;
        private String goodName;

        @Setter @Getter
        private List<String> list;

        public void setGoodId(String goodId) {
            this.goodId = goodId;
        }

        public String getGoodId() {
            return goodId;
        }

        public void setGoodName(String goodName) {
            this.goodName = goodName;
        }

        public String getGoodName() {
            return goodName;
        }


}
