package com.didispace.web.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 20:30
 * @Description:
 */
public class AuctionItemStateModel {
    @Setter
    @Getter
    private String auctionGoodId;//拍卖应用商品id
    @Setter
    @Getter
    private BigDecimal priceTimes;//竞价次数
    @Setter
    @Getter
    private BigDecimal dealPrice;//成交价格
    @Setter
    @Getter
    private long dealTime;//成交时间
    @Setter
    @Getter
    private String dealUserName;//得标人姓名
    @Setter
    @Getter
    private String State;//拍品状态
}
