package com.didispace.web.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/28 11:49
 * @Description:
 */
public class ImageModel {
    @Setter @Getter
    private String fileName;
    @Setter @Getter
    private String imageData;

}
