package com.didispace.util;

import com.didispace.domain.AuctionItemAttachment;
import com.didispace.domain.AuctionItemAttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.sql.Blob;
import java.util.UUID;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/24 19:49
 * @Description:
 */
public class Tools {

    private static String hexStr =  "0123456789ABCDEF";  //全局



    public static String getUUID(){
        String s = UUID.randomUUID().toString();
        s = s.substring(0,8)+s.substring(9,13)+s.substring(14,18)+s.substring(19,23)+s.substring(24);
        return s;
    }

    /**
     * 上传至文件目录
     * @param file
     * @return
     * @throws Exception
     */
    public static String executeUpload2Dir(MultipartFile file)throws Exception{
        //文件后缀名
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //上传文件名
        String fileName = UUID.randomUUID()+suffix;
        //服务端保存的文件对象
        File serverFile = new File(fileName);
        // 检测是否存在目录
        if (!serverFile.getParentFile().exists()) {
            serverFile.getParentFile().mkdirs();
        }
        //将上传的文件写入到服务器端文件内
        file.transferTo(serverFile);
        return fileName;
    }

    /**
     *
     * @param hexString
     * @return 将十六进制转换为字节数组
     */
    public static byte[] HexStringToBinary(String hexString){
        //hexString的长度对2取整，作为bytes的长度
        int len = hexString.length()/2;
        byte[] bytes = new byte[len];
        byte high = 0;//字节高四位
        byte low = 0;//字节低四位

        for(int i=0;i<len;i++){
            //右移四位得到高位
            high = (byte)((hexStr.indexOf(hexString.charAt(2*i)))<<4);
            low = (byte)hexStr.indexOf(hexString.charAt(2*i+1));
            bytes[i] = (byte) (high|low);//高地位做或运算
        }
        return bytes;
    }

    public static StringBuffer BinaryToHexString(byte[] bytes){

        StringBuffer result = new StringBuffer();
        String hex = "";
        for(int i=0;i<bytes.length;i++){
            hex = String.valueOf(hexStr.charAt((bytes[i]&0xF0)>>4));
            hex += String.valueOf(hexStr.charAt(bytes[i]&0x0F));
            result.append(hex);
        }
        return result;
    }

    /**
     * 将文件转为byte[]
     * @param filePath 文件路径
     * @return
     */
    public static byte[] getBytes(String filePath){
        File file = new File(filePath);
        ByteArrayOutputStream out = null;
        try {
            FileInputStream in = new FileInputStream(file);
            out = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int i = 0;
            while ((i = in.read(b)) != -1) {

                out.write(b, 0, b.length);
            }
            out.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] s = out.toByteArray();
        return s;

    }

    public static void writeFileUseByte(byte[] bytes,String fileName) throws IOException {
        //服务端保存的文件对象
        File serverFile = new File( fileName);

        OutputStream output = new FileOutputStream(serverFile);

        BufferedOutputStream bufferedOutput = new BufferedOutputStream(output);

        bufferedOutput.write(bytes);
    }

    public static byte[] StringDecode(String encodeString) throws IOException {
        BASE64Decoder base64Decoder = new BASE64Decoder();
        //解码
        byte[] decodeBuffer = base64Decoder.decodeBuffer(encodeString);

        return decodeBuffer;
    }
}
