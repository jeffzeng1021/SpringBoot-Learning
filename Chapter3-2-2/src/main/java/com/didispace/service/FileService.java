package com.didispace.service;

import com.didispace.domain.AuctionCndPictrure;
import com.didispace.domain.AuctionCndPictureRepository;
import com.didispace.domain.AuctionItemAttachment;
import com.didispace.domain.AuctionItemAttachmentRepository;
import com.didispace.util.DAO;
import com.didispace.util.Tools;
import oracle.sql.BLOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.util.Date;
import java.util.UUID;

/**
 * @Auther: jeffzeng
 * @Date: 2018/5/25 13:52
 * @Description:
 */
@Service
public class FileService {

    private static final String uploadDir = "D:\\";

    @Autowired
    AuctionItemAttachmentRepository auctionItemAttachmentRepository;

    @Autowired
    AuctionCndPictureRepository auctionCndPictureRepository;
    /**
     * 上传至oracle数据库
     *
     * @param auctionGoodId
     * @param file
     * @return
     * @throws Exception
     */
    public void executeUpload2Orcl(String auctionGoodId, MultipartFile file) throws Exception {
        //文件后缀名
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //上传文件名
        String fileName = UUID.randomUUID() + suffix;

        /*  Convert files to blob  */
        BLOB blob = BLOB.getEmptyBLOB();
        blob.setBytes(file.getBytes());
        //服务端保存的数据库
        AuctionItemAttachment auctionItemAttachment = new AuctionItemAttachment();
        auctionItemAttachment.setID(Tools.getUUID());
        auctionItemAttachment.setATTACHMENT(blob);
        auctionItemAttachment.setCONTENTTYPE(file.getContentType());
        auctionItemAttachment.setNAME(fileName);
        auctionItemAttachment.setITEMID(auctionGoodId);


        //将上传的文件写入到服务器端文件内
        auctionItemAttachmentRepository.save(auctionItemAttachment);
    }
    /**
     * 上传至oracle数据库
     *
     * @param auctionGoodId
     * @param imageByte
     * @return
     * @throws Exception
     */
    public void executeUpload2OrclUseByte(String auctionGoodId,String fileName, byte[] imageByte) throws Exception {
        //文件后缀名
//        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //上传文件名
//        String fileName = UUID.randomUUID() + suffix;

        /*  Convert files to blob  */
        BLOB blob = BLOB.getEmptyBLOB();
        blob.setBytes(imageByte);
        //服务端保存的数据库
//        AuctionItemAttachment auctionItemAttachment = new AuctionItemAttachment();
//        auctionItemAttachment.setID(Tools.getUUID());
//        auctionItemAttachment.setATTACHMENT(blob);
////        auctionItemAttachment.setCONTENTTYPE(file.getContentType());
//        auctionItemAttachment.setNAME(fileName);
//        auctionItemAttachment.setITEMID(auctionGoodId);

        AuctionCndPictrure auctionCndPictrure = new AuctionCndPictrure();
        auctionCndPictrure.setCOMPANYID(new BigDecimal(0));
        auctionCndPictrure.setITEMID(auctionGoodId);
        auctionCndPictrure.setPICTURE(blob);
        auctionCndPictrure.setPICTURENAME(fileName);
        auctionCndPictrure.setPICTUREID(Tools.getUUID());
        auctionCndPictureRepository.save(auctionCndPictrure);
        //将上传的文件写入到服务器端文件内
//        auctionItemAttachmentRepository.save(auctionItemAttachment);
    }

    /**
     * 在Oracle表blob字段的文件保存中，直接保存文件二进制内容不可行，需要先保存至目录中然后再从目录中获取文件流保存
     */
    public void uploadFile2OrclUseDir(String id,String fileName,byte[] bytes) throws Exception {
        //保存至本地目录
        Tools.writeFileUseByte(bytes,uploadDir+fileName);

        DAO dao = new DAO();

        dao.inertBlob2Orcl(id,uploadDir+fileName,bytes);

        File file = new File(uploadDir+fileName);
        if(file.delete()) {
            System.out.println( file.getName() + " is deleted!");
        }else {
            System.out.println("Delete operation is failed.");
        }
    }
}
